Metadata-Version: 2.1
Name: slepc4py
Version: 3.22.2
Summary: SLEPc for Python
Home-page: https://gitlab.com/slepc/slepc
Download-URL: https://pypi.io/packages/source/s/slepc4py/slepc4py-3.22.2.tar.gz
Author: Lisandro Dalcin
Author-email: dalcinl@gmail.com
Maintainer: SLEPc Team
Maintainer-email: slepc-maint@upv.es
License: BSD-2-Clause
Keywords: scientific computing,parallel computing,MPI,SLEPc,PETSc
Platform: POSIX
Platform: Linux
Platform: macOS
Platform: FreeBSD
Classifier: License :: OSI Approved :: BSD License
Classifier: Operating System :: POSIX
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Science/Research
Classifier: Programming Language :: C
Classifier: Programming Language :: C++
Classifier: Programming Language :: Cython
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Scientific/Engineering
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Development Status :: 5 - Production/Stable
Requires: numpy
Description-Content-Type: text/x-rst
License-File: LICENSE.rst
Requires-Dist: numpy
Requires-Dist: petsc4py<3.23,>=3.22

SLEPc for Python
================

Python bindings for SLEPc.

Install
-------

If you have a working MPI implementation and the ``mpicc`` compiler
wrapper is on your search path, it highly recommended to install
``mpi4py`` first::

  $ pip install mpi4py

Ensure you have NumPy installed::

  $ pip install numpy

and finally::

  $ pip install petsc petsc4py slepc slepc4py


Citations
---------

If SLEPc for Python been significant to a project that leads to an
academic publication, please acknowledge that fact by citing the
project.

* L. Dalcin, P. Kler, R. Paz, and A. Cosimo,
  *Parallel Distributed Computing using Python*,
  Advances in Water Resources, 34(9):1124-1139, 2011.
  https://doi.org/10.1016/j.advwatres.2011.04.013

* V. Hernandez, J.E. Roman and V. Vidal.
  *SLEPc: A Scalable and Flexible Toolkit for the
  Solution of Eigenvalue Problems*,
  ACM Transactions on Mathematical Software, 31(3):351-362, 2005.
  https://doi.org/10.1145/1089014.1089019
